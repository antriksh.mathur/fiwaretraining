#delete everything
echo 'db.entitiesdrop()' | mongo fiware-host --quiet

#delete as per type
echo 'db.entities.remove({"_id.type": "Person"})' | mongo orion --quiet | python -m json.tool


#read all entities
echo 'db.entities.find(})' | mongo orion --quiet
echo 'db.entities.find({"_id.type": "Person"})' | mongo orion --quiet | python -m json.tool